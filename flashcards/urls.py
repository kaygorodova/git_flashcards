from django.conf.urls import patterns, include, url

# from appflashcards import views
from appflashcards import urls as app_urls
from django.contrib import admin

# router = routers.DefaultRouter()
# router.register(r'users', views.UserViewSet)
# router.register(r'groups', views.GroupViewSet)

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'flashcards.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    # url(r'^', include(router.urls)),
    url(r"", include(app_urls.urlpatterns)),
)


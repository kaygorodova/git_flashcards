from django.test import TestCase
from rest_framework.test import APIClient
from appflashcards.serializers import CardSerializerForUser
from appflashcards.serializers import DeckSerializerForUser
from appflashcards.serializers import DeckDetailSerializerForUser
import json

from appflashcards.models import CustomUser, Deck, Card


class TestSuite(TestCase):
    def setUp(self):
        self.client = APIClient()


class DeckOfEmptyDatabaseTestCase(TestSuite):

    def setUp(self):
        super(DeckOfEmptyDatabaseTestCase, self).setUp()
        self.authUser = CustomUser.objects.create(email="someAuth@some.ru", name="foo")
        self.authUser.save()
        self.client.force_authenticate(self.authUser)

        self.notAuthUser = CustomUser.objects.create(email="someNotAuth@some.ru", name="foo")
        self.notAuthUser.save()


    def test_getDecks(self):
        for i in range(5):
            Deck.objects.create(name=str(i), user=self.authUser)

        for i in range(3):
            Deck.objects.create(name=str(i), user=self.notAuthUser)

        response = self.client.get("/api/decks/")
        self.assertEqual(response.status_code, 200)

        decks = Deck.objects.filter(user=self.authUser)
        self.assertEqual(decks.count(), 5)



    def test_createDeck(self):
        response = self.client.post("/api/decks/create/", {"name": "Test"})
        self.assertEqual(response.status_code, 201)
        deck = Deck.objects.get(pk=response.data["id"])

        decks = Deck.objects.filter(user=self.authUser)
        self.assertIn(deck, decks)

        self.assertEqual(decks.count(), 1)

        response = self.client.post("/api/decks/create/", {"test": "Test"})
        self.assertEqual(response.status_code, 400)

        self.assertEqual(decks.count(), 1)

        response = self.client.post("/api/decks/create/")
        self.assertEqual(response.status_code, 400)

        self.assertEqual(decks.count(), 1)

    def test_deckDoesNotExist(self):
        response = self.client.put("/api/decks/1000/update/")
        self.assertEqual(response.status_code, 404)

class DeckTestCase(TestSuite):

    def setUp(self):
        super(DeckTestCase, self).setUp()
        self.authUser = CustomUser.objects.create(email="someAuth@some.ru", \
            name="foo")
        self.authUser.save()
        self.client.force_authenticate(self.authUser)

        for i in range(2):
            deck = Deck.objects.create(user=self.authUser, name="Test" + str(i))
            for j in range(2):
                Card.objects.create(deck=deck, one_side=str(i) + str(j), \
                    another_side=str(j) + str(i))

        self.notAuthUser = CustomUser.objects.create(email="someNotAuth@some.ru", \
            name="foo")
        self.notAuthUser.save()

        for i in range(2, 4):
            deck = Deck.objects.create(user=self.notAuthUser, \
                name="Test" + str(i))
            for j in range(2, 4):
                Card.objects.create(deck=deck, one_side=str(i) + str(j), \
                    another_side=str(j) + str(i))


    def test_updateDeck(self):

        deck = Deck.objects.filter(user=self.authUser).order_by('?')[0]
        response = self.client.put("/api/decks/" + str(deck.id) + "/update/")
        self.assertEqual(response.status_code, 400)


        cards = Card.objects.filter(deck=deck)
        data = DeckDetailSerializerForUser(deck).data
        data["name"] = "Update"
        response = self.client.put("/api/decks/" + str(deck.id) + "/update/", \
            data, format="json")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(cards.count(), 2)
        self.assertEqual(response.data["name"], data["name"])


        data["cards"][0]["one_side"] = "test"
        response = self.client.put("/api/decks/" + str(deck.id) + "/update/", \
            data, format="json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(cards.count(), 2)
        self.assertEqual(sorted(response.data["cards"], \
            key=lambda card: card["id"]), sorted(data["cards"], \
            key=lambda card: card["id"]))

        data["cards"] = []
        response = self.client.put("/api/decks/" + str(deck.id) + "/update/", \
            data, format="json")
        self.assertEqual(response.status_code, 200)
        cards = Card.objects.filter(deck=deck)
        self.assertEqual(cards.count(), 0)

        data["cards"] = [{"one_side": "test1", "another_side": "test2"}]
        response = self.client.put("/api/decks/" + str(deck.id) + "/update/", \
            data, format="json")
        self.assertEqual(response.status_code, 200)
        cards = Card.objects.filter(deck=deck)
        self.assertEqual(cards.count(), 1)


        data = {"name": "Update", "cards": [{"one_side": 1, "another_side": 2}]}
        response = self.client.put("/api/decks/" + str(deck.id) + "/update/", \
            data, format="json")
        self.assertEqual(response.status_code, 200)
        cards = Card.objects.filter(deck=deck)
        self.assertEqual(cards.count(), 1)





        deck = Deck.objects.exclude(user=self.authUser).order_by('?')[0]
        response = self.client.put("/api/decks/" + str(deck.id) + "/update/")
        self.assertEqual(response.status_code, 403)


    def test_getDeck(self):
        deck = Deck.objects.filter(user=self.authUser).order_by('?')[0]
        response = self.client.get("/api/decks/" + str(deck.id) + "/")
        self.assertEqual(response.status_code, 200)

        deck = Deck.objects.exclude(user=self.authUser).order_by('?')[0]
        response = self.client.get("/api/decks/" + str(deck.id) + "/")
        self.assertEqual(response.status_code, 403)

    def test_removeDeck(self):
        deck_id = Deck.objects.filter(user=self.authUser).order_by('?')[0].id
        response = self.client.delete("/api/decks/" + str(deck_id) + "/remove/")
        self.assertEqual(response.status_code, 204)

        decks = Deck.objects.all()
        self.assertEqual(decks.count(), 3)

        cards = Card.objects.filter(deck=deck_id)
        self.assertEqual(cards.count(), 0)

        deck = Deck.objects.exclude(user=self.authUser).order_by('?')[0]
        response = self.client.delete("/api/decks/" + str(deck.id) + "/remove/")
        self.assertEqual(response.status_code, 403)






# Create your tests here.

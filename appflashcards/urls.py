from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

from appflashcards import views

url_api = patterns('',
    url(r"^users/create/$", views.create_user),
    url(r"^users/authentication$", views.authentication_user),
    url(r"^users/profile/$", views.profile),
    url(r"^users/profile/update/$", views.profile),
    url(r"^users/password/update/$", views.password),
    url(r"^users/avatar/update/$", views.avatar),
    url(r"^users/avatar/remove/$", views.avatar),
    url(r"^login/$", views.authentication_user),
    url(r"^decks/$", views.get_decks),
    url(r"^decks/(\d+)/$", views.deck_detail),
    url(r"^decks/create/$", views.deck_detail),
    url(r"^decks/(\d+)/update/$", views.deck_detail),
    url(r"^decks/(\d+)/remove/$", views.deck_detail),
    url(r"^decks/(\d+)/study/$", views.study_deck),
    url(r"^decks/(\d+)/study/update/$", views.study_deck)
    # other api
    # other api
)

url_include = patterns('', url(r"^(.*)/$", views.include))

directives = patterns('', url(r"^(.*)/$", views.directive))

urlpatterns = patterns('',
    url(r"^$", views.index),
    url(r"^include/", include(url_include)),
    url(r"^api/", include(url_api)),
    url(r"^api-token-auth/",
        "rest_framework.authtoken.views.obtain_auth_token"),
    url(r"^directives/", include(directives)),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from django.shortcuts import render
from appflashcards.serializers import UserSerializer, LoginSerializer
from appflashcards.serializers import DeckSerializerForUser, DeckSerializer
from appflashcards.serializers import DeckDetailSerializer, DeckDetailSerializerForUser
from appflashcards.serializers import CardSerializer, CardSerializerForUser
from appflashcards.serializers import StudySerializer, PasswordUpdateSerializer
from appflashcards.serializers import CustomUserForUserSerializer
from appflashcards.serializers import AvatarSerializer
from django.core.files.base import ContentFile

from appflashcards.models import CustomUser, Deck, Card
from rest_framework.authentication import TokenAuthentication
from django.http import HttpResponse, Http404
from rest_framework.authtoken.models import Token
from django.template import TemplateDoesNotExist
# from serializers import UserSerializer
from rest_framework import status, permissions
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from datetime import date, timedelta

import os
import math
import json
import random
import pdb

def error(data):
    return Response(data, status=status.HTTP_400_BAD_REQUEST)

def index(request):
    return render(request, "base.html")


def include(request, name):
    try:
        return render(request, "include/{}.html".format(name))
    except TemplateDoesNotExist:
        raise Http404

def create_user(request):
    user = json.loads(request.body.decode('utf-8'))
    ser = UserSerializer(data=user)
    if ser.is_valid():
        data = ser.object
        u = CustomUser.objects.create_user(email=data["email"],
            password=data["password1"])
        u.name = data["username"]
        u.save()
        Token.objects.create(user=u)
        return HttpResponse("ok")
    else:
        return HttpResponse(json.dumps(ser.errors), status=400)

@api_view(['GET'])
def authentication_user(request):
    # user = json.loads(request.body.decode('utf-8'))
    ser = LoginSerializer(data=request.QUERY_PARAMS)
    if ser.is_valid():
        user = ser.object['user']
        return Response({"name": user.name, "token": user.auth_token.key})
        # return HttpResponse("ok")
    else:
        return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)


def directive(request, name):
    try:
        return render(request, "directive/{}.html".format(name))
    except TemplateDoesNotExist:
        raise Http404

@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def get_decks(request):
    decks = Deck.objects.filter(user=request.user)
    return Response(DeckSerializerForUser(decks, many=True).data)

@api_view(['GET', 'PUT'])
@authentication_classes((TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def study_deck(request, pk):
    data = request.DATA.copy()

    try:
        deck = Deck.objects.get(pk=pk)
    except Deck.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == "GET":
        cards = Card.objects.filter(deck=deck).exclude(date__gt=date.today())[0:15]
        cards = sorted(cards, key=lambda x: random.random())
        return Response(CardSerializerForUser(cards, many=True).data)

    elif request.method == "PUT":
        ser = StudySerializer(data=data)
        if not ser.is_valid():
            return HttpResponse(ser.errors, status=400)

        cards = Card.objects.filter(pk__in=(list(map(lambda x: x["id"], data))), deck__user=request.user)
        if len(data) != cards.count():
            return HttpResponse(status=400)

        dic = dict(list(map(lambda x: (x["id"], x["res"]), data)))


        for card in cards:
            if dic[card.pk]:
                card.q = min(5, card.q + 1)
            else:
                card.q = max(1, card.q - 1)

            card.ef += (0.1 - (5 - card.q) * (0.08 + (5 - card.q) * 0.02))

            if card.ef < 1.3:
                card.ef = 1.3
            elif card.ef > 2.5:
                card.ef = 2.5

            card.repetition += 1

            if card.repetition == 1:
                card.interval = 1
            elif card.repetition == 2:
                card.interval = 6
            else:
                card.interval = math.trunc(card.interval * card.ef)

            card.date = date.today() + timedelta(card.interval)

            card.save()

        return Response("ok")






@api_view(['POST', 'PUT', 'GET', 'DELETE'])
@authentication_classes((TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def deck_detail(request, pk=None):

    data = request.DATA.copy()

    if request.method == "POST":
        data["user"] = request.user.pk
        ser = DeckSerializer(data=data)
        if ser.is_valid():
            ser.save()
            data = ser.data
            del data['user']
            return Response(data, status=201)
    else:

        try:
            deck = Deck.objects.get(pk=pk)
        except Deck.DoesNotExist:
            return HttpResponse(status=404)

        if deck.user.pk != request.user.pk:
            return Response("Requested a deck of another user", status=403)

        if request.method == "PUT":
            ser = DeckDetailSerializerForUser(deck, data=data)
            if ser.is_valid():
                ser.save()
                return Response(ser.data, status=200)

        elif request.method == "GET":
            return Response(DeckDetailSerializerForUser(deck).data)

        elif request.method == "DELETE":
            deck.delete()
            return HttpResponse("Deck removed", status=204)

    return error(ser.errors)

@api_view(['PUT', 'GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def profile(request):

    user = CustomUser.objects.get(pk=request.user.pk)
    if request.method == "GET":
        return Response(CustomUserForUserSerializer(user).data)

    elif request.method == "PUT":
        data = request.DATA.copy()
        data["user"] = request.user.pk

        ser = CustomUserForUserSerializer(data=data)

        if ser.is_valid():
            user.name = ser.data["name"]
            user.save()
            return Response(ser.data, status=200)
    return error(ser.errors)


@api_view(['PUT'])
@authentication_classes((TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def password(request):
    user = CustomUser.objects.get(pk=request.user.pk)
    data = request.DATA.copy()


    ser = PasswordUpdateSerializer(data=data)
    if ser.is_valid():
        if not user.check_password(ser.data["password_old"]):
            return error({"password_old": ["Неверный пароль"]})
        else:
            user.set_password(ser.data["password1"])
            user.save()
            return Response(status=200)

    return error(ser.errors)

@api_view(['POST', 'DELETE'])
@authentication_classes((TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def avatar(request):

    def getPathAvatars(path):
        index = -1

        while path[index] != "/":
            index -= 1

        return path[:index]

    def removeFilesAvatar(user):
        path_avatar_medium = user.avatar_medium.path
        os.remove(user.avatar.path)
        os.remove(path_avatar_medium)

        try:
            os.removedirs(getPathAvatars(path_avatar_medium))
        except os.OSError:
            pass


    if request.method == "POST":
        ser = AvatarSerializer(files=request.FILES)

        if ser.is_valid():

            user = CustomUser.objects.get(pk=request.user.pk)
            file_content = ContentFile(ser.object.avatar.file.read())
            if user.avatar:
                removeFilesAvatar(user)
            user.avatar.save(ser.object.avatar.name, file_content)
            user.save()
            return Response({'avatarUrl': user.avatar_medium.url}, status=200)

    elif request.method == "DELETE":
        user = CustomUser.objects.get(pk=request.user.pk)
        if user.avatar:
            removeFilesAvatar(user)
            user.avatar.delete()
            user.save()
        return HttpResponse("Avatar removed", status=204)


    return error(ser.errors)



















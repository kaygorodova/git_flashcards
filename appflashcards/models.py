from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)
from django.utils.translation import ugettext as _
import PIL
from PIL import Image
from imagekit.models.fields import ImageSpecField
from imagekit import ImageSpec, register
from imagekit.processors import ResizeToFit, Adjust,ResizeToFill

class CustomUserManager(BaseUserManager):
    def create_user(self, email, password=None):
        if not email:
            msg = 'Users must have an email address'
            raise ValueError(msg)

        user = self.model(
            email=CustomUserManager.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self,
                            email,
                            favorite_pet,
                            password):
        user = self.create_user(email,
            password=password,
        )
        # user.is_admin = True
        user.is_staff = True
        # user.is_superuser = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=100)
    is_staff = models.BooleanField(default=False)
    avatar = models.ImageField(null=True, upload_to="avatars")

    avatar_medium = ImageSpecField(source='avatar',
        processors=[ResizeToFill(width=300, height=300, upscale=True)])

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return "name = {}, email = {}".format(self.name, self.email)

class Deck(models.Model):
    class Meta:
        verbose_name = _('Deck')
        verbose_name_plural = _('Decks')

    def __unicode__(self):
        pass

    name = models.CharField(max_length=50)
    user = models.ForeignKey(CustomUser)

class Card(models.Model):
    class Meta:
        verbose_name = _('Card')
        verbose_name_plural = _('Cards')
        ordering = ["date", "-ef"]

    def __unicode__(self):
        pass

    one_side = models.CharField(max_length=200)
    another_side = models.CharField(max_length=200)
    deck = models.ForeignKey(Deck)
    repetition = models.IntegerField(default=0)
    interval = models.IntegerField(default=0)
    ef = models.FloatField(default=2.5)
    date = models.DateField(null=True, blank=True)
    q = models.IntegerField(default=0)




from django.contrib.auth import authenticate
from rest_framework import serializers
from appflashcards.models import CustomUser, Deck, Card

class CustomUserForUserSerializer(serializers.ModelSerializer):
    avatarUrl = serializers.SerializerMethodField('getAvatarUrl')

    def getAvatarUrl(self, obj):
        if obj.avatar:
            return obj.avatar_medium.url
        return ""

    class Meta:
        model = CustomUser
        fields = ('name', 'avatarUrl')

class CustomUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser

class PasswordUpdateSerializer(serializers.Serializer):
    password_old = serializers.CharField()
    password1 = serializers.CharField()



class UserSerializer(serializers.Serializer):
    email = serializers.EmailField()
    username = serializers.CharField(max_length=30)
    password1 = serializers.CharField()

    def validate(self, attrs):
        if CustomUser.objects.filter(email=attrs['email']):
            raise serializers.ValidationError(
                "Пользователь с таким E-mail существует")
        return attrs


class AvatarSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('avatar',)


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self, attrs):
        user = authenticate(email=attrs['email'], password=attrs['password'])
        if not user:
            raise serializers.ValidationError(
                "Неверный пользователь или пароль")
        attrs['user'] = user
        return attrs



class DeckSerializer(serializers.ModelSerializer):
    """docstring for DeckSerializer"""
    class Meta:
        model = Deck

class DeckSerializerForUser(serializers.ModelSerializer):
    class Meta:
        model = Deck
        fields = ('id', 'name')


class CardSerializer(serializers.ModelSerializer):
    """docstring for CardSerializer"""
    class Meta:
        model = Card

class CardSerializerForUser(serializers.ModelSerializer):
    class Meta:
        model = Card
        fields = ('id', 'one_side', 'another_side')


class DeckDetailSerializer(serializers.ModelSerializer):
    cards = CardSerializerForUser(many=True, source="card_set", allow_add_remove=True)
    class Meta:
        model = Deck

class DeckDetailSerializerForUser(serializers.ModelSerializer):
    cards = CardSerializerForUser(many=True, source="card_set", allow_add_remove=True)
    class Meta:
        model = Deck
        fields = ('name', 'cards')

class StudySerializer(serializers.Serializer):
    id = serializers.IntegerField()
    res = serializers.BooleanField()









angular.module('flashcards')

.directive('inputValidate', [function(){
    // Runs during compile
    return {
        // name: '',
        // priority: 1,
        // terminal: true,
        scope: {
            "isSubmited": "="
        }, // {} = isolate, true = child, false/undefined = no change
        // controller: function($scope, $element, $attrs, $transclude) {},
        require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
        restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
        // template: '',
        // templateUrl: '',
        // replace: true,
        // transclude: true,
        // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
        link: function($scope, iElm, iAttrs, controller) {
            var class_name = "has-error";
            var parent = iElm.parent();
            var change = function () {
                if(!controller.$valid && $scope.isSubmited) {
                    parent.addClass(class_name);
                }
                else {
                    parent.removeClass(class_name);
                }
            };
            $scope.$watchCollection(function () { return [
                $scope.isSubmited, controller.$viewValue, controller.$valid];},
                change);
    }
};}])

.directive('formErrors', [function(){
    // Runs during compile
    return {
        // name: '',
        // priority: 1,
        // terminal: true,
        scope: {
            "serverErrors": "=errors",
            "fieldName": "@",
            "isSubmited": "="
        }, // {} = isolate, true = child, false/undefined = no change
        // controller: function($scope, $element, $attrs, $transclude) {},
        require: '^form', // Array = multiple requires, ? = optional, ^ = check parent elements
        restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
        templateUrl: "/directives/errors",
        // templateUrl: '',
        replace: true,
        // transclude: true,
        // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
        link: function($scope, iElm, iAttrs, controller) {
            $scope.error_list = {
                "required": "Это поле обязательно",
                "email": "Неправильный адрес",
                "confirm": "Пароли должны совпадать"
            };
            $scope.$watch('fieldName', function () {
                $scope.field = controller[$scope.fieldName];
            });
        }};
}])

.directive('confirm', [function(){
    // Runs during compile
    return {
        // name: '',
        // priority: 1,
        // terminal: true,
        // scope: false, // {} = isolate, true = child, false/undefined = no change
        // controller: function($scope, $element, $attrs, $transclude) {},
        require: '?ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
        restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
        // template: '',
        // templateUrl: '',
        // replace: true,
        // transclude: true,
        // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
        link: function($scope, iElm, iAttrs, ngModel) {
            if(!ngModel) return;
            ngModel.$validators.confirm = function (new_value) {
                var value = ngModel.$viewValue || ngModel.$modelValue;
                return value === iAttrs.confirm;
            }
            iAttrs.$observe("confirm", function (val) {
                ngModel.$validate();
            })
        }
    };
}])
.directive('fileModel', ['$parse', function($parse){
    // Runs during compile
    return {
        // name: '',
        // priority: 1,
        // terminal: true,
        scope: false, // {} = isolate, true = child, false/undefined = no change
        // controller: function($scope, $element, $attrs, $transclude) {},
        //require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
        restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
        // template: '',
        // templateUrl: '',
        // replace: true,
        // transclude: true,
        // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
        link: function($scope, iElm, iAttrs) {
            var model = $parse(iAttrs.fileModel);
            var modelSetter = model.assign;

            iElm.bind('change', function(){
                $scope.$apply(function(){
                    modelSetter($scope, iElm[0].files[0]);
                });
            });

        }
    };
}])


angular.module('flashcards')

.controller('index', ['$scope', '$http', function($scope, $http) {
    $scope.some = "Hello world";
}])

.controller('registration', ['$scope', '$http', function($scope, $http){
    $scope.user = {};
    $scope.submited = false;
    $scope.errors = {};
    $scope.is_success = false;


    $scope.submit = function(form) {
        $scope.submited = true;
        if(form.$valid) {
            $http.post('api/users/create/', $scope.user)
            .success(function (data) {
                console.log(data);
                $scope.is_success = true;
            })
            .error(function (data) {
                $scope.errors = data;
            });

        }

    };

}])

.controller('login', ['$scope', 'auth', '$state', function($scope, auth, $state){
    $scope.user = {};
    $scope.submited = false;
    $scope.submit = function (form) {
        $scope.submited = true;
        if(form.$valid) {
            var user = $scope.user;
            var promise = auth.authenticate(user.email, user.password);
            promise.success(function (data) {
                console.log("ok");
                $state.go("index");
            })
            .error(function (errors) {
                $scope.errors = errors;
            })
        }
    }


}])
.controller('auth', ['auth', '$scope', function(auth, $scope){
    $scope.auth = auth;
}])

.controller('editing', ['$scope', '$stateParams', '$http', '$state',
    function($scope, $stateParams, $http, $state){
    $scope.data = {}
    $scope.submited = false;
    var id = $stateParams.id;
    var init = function() {
            $scope.is_create = false;
            $http.get("/api/decks/" + id + '/')
            .success(function (data) {
                $scope.data = data;
            })

    };

    init();

    $scope.newCard = function() {
        $scope.data.cards.push({"one_side": "", "another_side": ""});
        $scope.submited = false;
    };

    $scope.saveDeck = function(form) {
        $scope.submited = true;
        if(form.$valid) {
            $http.put('/api/decks/' + id + '/update/', $scope.data)
            .error(function (data) {
                console.log(data);
            })
            .success(function (data) {
                $scope.data.cards = data.cards;
            });

        };

    };

    $scope.removeCard = function(card) {
        index = $scope.data.cards.indexOf(card);
        $scope.data.cards.splice(index, 1);
    };

}])

.controller('study', ['$scope', '$http', '$stateParams', '$state',
    function($scope, $http, $stateParams, $state){
    var id = $stateParams.id;
    var answers = [];
    $scope.index;
    $scope.cards = [];
    $scope.data = {answer:""};
    $scope.question = true;
    $scope.is_end;
    $scope.not_cards;
    var init = function () {

        $http.get('/api/decks/' + id + '/study/')
        .success(function (data) {
            $scope.cards = data;
            $scope.index = 0;
            if($scope.cards.length == 0) {
                $scope.is_end = true;
                $scope.not_cards = true;
            }
            else {
                $scope.is_end = false;
                $scope.not_cards = false;
            }
        })

    }
    init();

    $scope.check = function () {
        var id = $scope.cards[$scope.index].id
        var res = ($scope.data.answer == $scope.cards[$scope.index].another_side)
        answers.push({id: id, res: res})
        $scope.question = false;

    };

    $scope.next = function () {
        $scope.index += 1;
        $scope.data.answer = "";
        $scope.question = true;
        if($scope.cards.length <= $scope.index) {
            $scope.is_end = true;
            $http.put('/api/decks/' + id + '/study/update/', answers)
            .success(function (data) {
                ;
            })
        }
    }

}])

.controller('decks', ['$scope', '$http', '$modal', '$state',
    function($scope, $http, $modal, $state){
    var init = function () {
        $http.get("/api/decks/")
        .success(function (data) {
            $scope.decks = data;
        })
    }
    init();

    $scope.createDeck = function () {
        modalInstance = $modal.open({
            templateUrl: '/include/create/',
            controller: 'createDeck'
        });

        modalInstance.result.then(function(deck) {
            $scope.decks.push(deck);
        })

    }
    $scope.removeDeck = function (deck) {
        modalInstance = $modal.open({
            templateUrl: '/include/remove/',
            controller: 'removeDeck',
            resolve: {
                deck: function () {
                    return deck;
                }
            }
        })

        modalInstance.result.then(function (deck) {
            index = $scope.decks.indexOf(deck);
            $scope.decks.splice(index, 1);
        })

    }
}])
.controller('removeDeck', ['$scope', '$modalInstance', '$http', 'deck',
    function($scope, $modalInstance, $http, deck){
    $scope.deck = deck;

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }

    $scope.submit = function () {

        $http.delete('/api/decks/' + $scope.deck.id + '/remove/')
        .success(function (data) {
            $modalInstance.close($scope.deck);
        })
        .error(function (data) {
            console.log(data);
        });
    }


}])
.controller('createDeck', ['$scope', '$modalInstance', '$http',
    function($scope, $modalInstance, $http){
    $scope.submited = false;
    $scope.data = {};
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.submit = function (form) {
        $scope.submited = true;
        if(form.$valid) {
            $http.post('/api/decks/create/', $scope.data)
            .success(function (data) {
                $modalInstance.close(data);
            })
            .error(function (data) {
                console.log(data)
                $modalInstance.dismiss('error')
            });
        }
    };

}])

.controller('profile', ['$scope', '$http', 'auth', '$modal', 'fileUpload',
    function($scope, $http, auth, $modal, fileUpload){
    $scope.submited = false;
    user = auth.isAuthenticated()
    $scope.myFile = null;

    var init = function (){
        $scope.avatarUrl = "/static/images/default-avatar.png"

        $http.get('/api/users/profile/')
        .success(function (data) {
            $scope.user = data
            $scope.user.email = user.email
            if($scope.user.avatarUrl == ""){
                $scope.user.avatarUrl = "/static/images/default-avatar.png"

            }

        })
        .error(function (data) {
            console.log(data)
        })

    }
    init()

    $scope.save = function(form) {
        $scope.submited = true;
        if(form.$valid) {
            $http.put('/api/users/profile/update/', $scope.user)
            .error(function (data) {
                console.log(data);
            })
            .success(function (data) {
                console.log("ok")

                $scope.user = data

                user.name = data.name
                auth.updateUser(user)

                modalInstance = $modal.open({
                templateUrl: '/include/message/',
                controller: 'message',
                resolve: {
                    text: function () {
                        return "Изменения сохранены";
                    }
                }

                });

            });
        }

    };

    $scope.updatePassword = function() {
        modalInstance = $modal.open({
            templateUrl: '/include/password/',
            controller: 'password'
        })
    };

    $scope.uploadAvatar = function(){
        if($scope.myFile){
            var file = $scope.myFile;
            var uploadUrl = "/api/users/avatar/update/";

            var fd = new FormData();
            fd.append('avatar', file);
            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
            .success(function(data){
                $scope.user.avatarUrl = data.avatarUrl
            })
            .error(function(data){
                console.log(data);
            });
        }
    };
    $scope.$watch('myFile', $scope.uploadAvatar);

    $scope.removeAvater = function(){
        $http.delete("/api/users/avatar/remove/")
        .error(function(data){
            console.log(data)
        })
        .success(function(data){
            $scope.user.avatarUrl = "/static/images/default-avatar.png"
        });

    };
}])
.controller('message', ['$scope', '$modalInstance', 'text',
    function($scope, $modalInstance, text){
        $scope.text = text;

        $scope.submit = function() {
            $modalInstance.close()
        };

}])
.controller('password', ['$scope', '$modalInstance', '$http', '$modal',
    function($scope, $modalInstance, $http, $modal){
        $scope.submited = false;
        $scope.user = {};
        $scope.errors = {};

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.submit = function(form) {
            $scope.submited = true;


            if(form.$valid) {
                $http.put('/api/users/password/update/', $scope.user)
                .error(function (data) {
                    $scope.errors = data;
                    console.log(data);
                })
                .success(function () {
                    console.log("ok");
                    $modalInstance.close()
                    modalInstance = $modal.open({
                    templateUrl: '/include/message/',
                    controller: 'message',
                    resolve: {
                        text: function () {
                            return "Пароль изменен";
                        }

                    }
                    });


                });

            };

        };
}])



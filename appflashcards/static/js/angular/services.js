var flashcards = angular.module('flashcards');

flashcards.factory('activeUser', ['$window', function($window){
    return {
        getUser: function () {
            var user = JSON.parse($window.localStorage.getItem('user'));
            return user;

        },
        setUser: function (user) {
            $window.localStorage.setItem('user', JSON.stringify(user));

        },
        removeUser: function () {
            $window.localStorage.removeItem('user');

        }

    };
}])
.factory('auth', ['$http', 'activeUser', '$q',
    function($http, activeUser, $q) {
    var url = "/api/login/";
    var user = activeUser.getUser();
    if (!user) {
        user = undefined;
    }
    return {
        isAuthenticated: function () {
            return user || false;
        },
        authenticate: function(email, password) {
            return $http.get(url, {params: {email: email, password: password}})
            .success(function (data) {
                user = {
                    email: email,
                    name: data.name,
                    token: data.token,
                }
                activeUser.setUser(user);
            });
        },
        logout: function () {
                activeUser.removeUser();
                user = undefined;
        },
        updateUser: function(newUser) {
            user.name = newUser.name
            activeUser.setUser(user)
        }
    };
}])
.factory('authInterceptor', ['$q', '$location', 'activeUser',
    function($q, $location, activeUser) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            var user = activeUser.getUser();
            if(user) {
                config.headers.Authorization = 'Token ' + user.token;
            }
            return config;
        },
        responseError: function (response) {
            if(response.status === 401 || response === 403) {
                $location.path('/login/');
            };
            return $q.reject(response);

        }
    }
}])

.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(name, file, uploadUrl){
        var fd = new FormData();
        fd.append(name, file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(){
        })
        .error(function(){
        });
    }
}]);


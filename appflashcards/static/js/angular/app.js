var flashcards = angular.module('flashcards', ['ui.router', 'ui.bootstrap'])

flashcards.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
    $urlRouterProvider.otherwise("/404/");
    $stateProvider
        .state("registration", {
            url: "/registration/",
            templateUrl: "/include/registration/",
            controller: "registration"
        })
        .state("login", {
            url: "/login/",
            templateUrl: "/include/login/",
            controller: "login"
        })
        .state("index", {
            url: "/",
            templateUrl: "/include/index/"
        })
        .state("editing", {
            url: "/editing/:id",
            templateUrl: "/include/editing/",
            controller: "editing"
        })
        .state("study", {
            url: "/study/:id",
            templateUrl: "/include/study/",
            controller: "study"
        })
        .state("404", {
            url: "/404/",
            templateUrl: "/include/404/"
        })
        .state("decks", {
            url: "/decks/",
            templateUrl: "/include/decks/",
            controller: "decks"
        })
        .state("profile", {
            url: "/profile/",
            templateUrl: "/include/profile/",
            controller: "profile"
        })
})